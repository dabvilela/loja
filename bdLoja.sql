
/*criando base*/
create database loja;

use loja;
/*Tabela Login*/
create table login (id int identity(1,1) not null primary key, user_login VARCHAR(15) not null, senha VARCHAR(15) not null );

insert into login (user_login, senha) 
        values ('admin', 'admin');

/*tabela cliente*/
create TABLE cliente (codigo_cliente int identity(1,1) not null primary key, nome_cliente varchar(150)not null , endereco_cliente varchar(150) 
                        ,telefone varchar (20), email varchar(150), data_nascimento date);

insert INTO cliente (nome_cliente, endereco_cliente, telefone, email, data_nascimento)
            VALUES ('Daniel','Rua 21','12988549347','dabvilela@gmail.com','1997-10-13' );

insert INTO cliente (nome_cliente, endereco_cliente, telefone, email, data_nascimento)
            VALUES ('Seimour','Rua 22','12988549347','usuario1@gmail.com','1984-03-05' );

insert INTO cliente (nome_cliente, endereco_cliente, telefone, email, data_nascimento)
            VALUES ('Fabiana','Rua 23','12988549347','usuario2@gmail.com','1997-10-13');

insert INTO cliente (nome_cliente, endereco_cliente, telefone, email, data_nascimento)
            VALUES ('isabella','Rua 24','12988549347','usuario3@gmail.com','1997-10-13' );

insert INTO cliente (nome_cliente, endereco_cliente, telefone, email, data_nascimento)
            VALUES ('isabela','Rua 25','12988549347','usuario4@gmail.com','1997-10-13' ); 


/*tabela produtos*/

create TABLE produtos (codigo_produto int identity(1,1) not null PRIMARY KEY, descricao_produto varchar(150) not null, preco_venda FLOAT NOT NULL,
                        preco_custo FLOAT not NULL, estoque int not null, data_vencimento date );

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Lampada comum', 5.00, 2.0, 150, '13/05/2022');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Lampada led', 10.00, 4.0, 100, '13/05/2024');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Leite', 4.20, 1.50, 250, '14/12/2020');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Caixa bonbon', 8.00, 3.50, 180, '19/05/2021');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('bolo pulmman', 5.00, 1.50, 250, '17/06/2021');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Feijao 1 kg', 4.90, 2.0, 150, '13/05/2021');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Arroz', 12.00, 2.0, 150, '15/04/2021');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Marcarrão', 7.00, 3.0, 350, '13/05/2022');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Queijo gorgonzola ', 15.00, 5.0, 50, '11/01/2021');

insert into produtos (descricao_produto, preco_venda, preco_custo, estoque, data_vencimento)
        VALUES('Vinho', 50.00, 25.0, 350, '19/05/2025');

/*tabela fornecedor*/

create table fornecedor (codigo_fornecedor int identity(1,1) not null primary key, nome_fornecedor varchar(150) not null,
                          razao_social_fornecedor varchar(150), telefone_forn varchar(20), email_forn  varchar(20) );

INSERT into fornecedor (nome_fornecedor, razao_social_fornecedor, telefone_forn, email_forn)
        VALUES ('Atacadão','Atacadão','39075687','atacadao@atacadista.com');
