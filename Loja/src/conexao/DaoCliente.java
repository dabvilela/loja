/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import cadastros.Cliente;

/**
 *
 * @author dabvi
 */
public class DaoCliente {

    
    public List<Cliente> showgrade(){
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Cliente> clientes = new ArrayList<>();
        
        
        try {
            
            statement = conexao.prepareStatement("SELECT * FROM cliente");           
            result = statement.executeQuery();
            
            while (result.next()){
                Cliente cliente = new Cliente();
                
                cliente.setCodigo_cliente(result.getInt("codigo_cliente"));
                cliente.setNome(result.getString("nome_cliente"));
                cliente.setTelefone(result.getString("telefone"));
                cliente.setData_nascimento(result.getDate("data_nascimento"));
                cliente.setEmail(result.getString("email"));
                cliente.setEndereco(result.getString("endereco_cliente"));
                clientes.add(cliente);
                
            }
            
           // JOptionPane.showMessageDialog(null, "Programa carregado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao carregar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientes;
       
    }
        
    
    
    public void create(Cliente cliente){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("INSERT INTO cliente (nome_cliente,telefone,data_nascimento,email,endereco_cliente)VALUES(?,?,?,?,?)");            
            statement.setString(1,cliente.getNome());
            statement.setString(2, cliente.getTelefone());
            statement.setDate(3, (Date) cliente.getData_nascimento());
            statement.setString(4, cliente.getEmail());
            statement.setString(5, cliente.getEndereco());
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "salvo com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao salvar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
    public void update(Cliente cliente){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        
        try {
            
            statement = conexao.prepareStatement
            ("UPDATE cliente SET nome_cliente =?,telefone=?,data_nascimento=?,email = ?,endereco_cliente=? where codigo_cliente=?");
            
            statement.setString(1,cliente.getNome());           
            statement.setString(2, cliente.getTelefone());
            statement.setDate(3, (Date) cliente.getData_nascimento());
            statement.setString(4,cliente.getEmail());
            statement.setString(5,cliente.getEndereco());
            statement.setInt(6, cliente.getCodigo_cliente());
            
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao Atualiazar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
    public void delete (Cliente cliente){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("Delete from cliente where codigo_cliente=?");
            statement.setInt(1,cliente.getCodigo_cliente());
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "Excluido com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao Excluir");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
    
    
    public List<Cliente> searchNome(String nome){
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Cliente> clientes = new ArrayList<>();
        
        
        try {
            
            statement = conexao.prepareStatement("SELECT * FROM cliente WHERE nome_cliente LIKE ?");  
            statement.setString(1,"%"+nome+"%");           
            result = statement.executeQuery();
            
            while (result.next()){
                Cliente cliente = new Cliente();
                
                cliente.setCodigo_cliente(result.getInt("codigo_cliente"));
                cliente.setNome(result.getString("nome_cliente"));
                cliente.setTelefone(result.getString("telefone"));
                cliente.setData_nascimento(result.getDate("data_nascimento"));
                cliente.setEmail(result.getString("email"));
                cliente.setEndereco(result.getString("endereco_cliente"));
                
                clientes.add(cliente);
                
            }
            
           // JOptionPane.showMessageDialog(null, "Programa carregado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao buscar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientes;
       
    }

    
   
}
