/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;

import cadastros.Produto;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author dabvi
 */
public class DaoProduto {
    
    public List<Produto> showgradeProduto(){
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Produto> produtos = new ArrayList<>();
        
        
        try {
            
            statement = conexao.prepareStatement("SELECT * FROM produtos");           
            result = statement.executeQuery();
            
            while (result.next()){
                Produto produto = new Produto();
                
                produto.setCodigo_produto(result.getInt("codigo_produto"));
                produto.setDescricao(result.getString("descricao_produto"));
                produto.setData_validade(result.getDate("data_vencimento"));
                produto.setPreco_custo(result.getInt("preco_custo"));
                produto.setPreco_venda(result.getInt("preco_venda"));
                produto.setEstoque(result.getInt("estoque"));
                
                produtos.add(produto);
                
            }
            
           // JOptionPane.showMessageDialog(null, "Programa carregado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao carregar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return produtos;       
    }
    
     public List<Produto> showgradeProdutoBusca(String nome){
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Produto> produtos = new ArrayList<>();
        
        
        try {
            
            statement = conexao.prepareStatement("SELECT * FROM produtos WHERE descricao_produto LIKE ?");  
            statement.setString(1,"%"+nome+"%");           
            result = statement.executeQuery();
            
            while (result.next()){
                Produto produto = new Produto();
                
                produto.setCodigo_produto(result.getInt("codigo_produto"));
                produto.setDescricao(result.getString("descricao_produto"));
                produto.setData_validade(result.getDate("data_vencimento"));
                produto.setPreco_custo(result.getInt("preco_custo"));
                produto.setPreco_venda(result.getInt("preco_venda"));
                produto.setEstoque(result.getInt("estoque"));
                
                produtos.add(produto);
                
            }
            
           // JOptionPane.showMessageDialog(null, "Programa carregado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao carregar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return produtos;
     }
     
     public void createProduto(Produto produto){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("INSERT INTO produtos (descricao_produto, preco_custo,preco_venda,estoque,data_vencimento) VALUES(?,?,?,?,?)");            
            statement.setString(1,produto.getDescricao());            
            statement.setDouble(2, produto.getPreco_custo());
            statement.setDouble(3, produto.getPreco_venda());
            statement.setDouble(4, produto.getEstoque());
            statement.setDate(5, (Date) produto.getData_validade());
           
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "salvo com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao salvar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
     
     public void UpdateProduto(Produto produto){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("UPDATE produtos SET descricao_produto =?,preco_custo=?,preco_venda=?,estoque = ?,data_vencimento=? where codigo_produto=?");            
            statement.setString(1,produto.getDescricao());            
            statement.setDouble(2, produto.getPreco_custo());
            statement.setDouble(3, produto.getPreco_venda());
            statement.setDouble(4, produto.getEstoque());
            statement.setDate(5, (Date) produto.getData_validade());
            statement.setInt(6,produto.getCodigo_produto());
           
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "salvo com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao salvar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
     
    public void deleteProduto(Produto produto){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("DELETE from produtos where codigo_produto=?");
            statement.setInt(1,produto.getCodigo_produto());
                        
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "exluido com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao excluir");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    } 
}
