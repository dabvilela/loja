/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author dabvi
 */
public class ConexaoBD {
           
    private static final String DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String URL = "jdbc:sqlserver://localhost:1433;databaseName=loja";
    private static final String USER = "sa";
    private static final String PASS = "1234";
    
    public static Connection criarConexao(){
    
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USER, PASS);

        } catch (ClassNotFoundException ex) {

            throw new RuntimeException("Erro na conexão" + ex);
        } catch (SQLException ex) {
            throw new RuntimeException("Erro na conexão" + ex);
        }

    }
    
    public static void fechaConexao (Connection conexao) {

        if (conexao != null) {
            try {
                conexao.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Erro ao fechar conexão" + ex);
            }
        }
    }

    public static void fechaConexao(Connection conexao, PreparedStatement statement) {

        fechaConexao(conexao);
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao fechar conexão" + ex);
        }
    }

    public static void fechaConexao(Connection conexao, PreparedStatement statement, ResultSet result) {

        fechaConexao(conexao, statement, result);

        try {
            if (result != null) {
                result.close();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao fechar conexão" + ex);
        }
    }
    
}
