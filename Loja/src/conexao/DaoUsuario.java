/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;


import cadastros.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author dabvi
 */
public class DaoUsuario {
    
    
    
    public List<Usuario> userGrade(){
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Usuario> usuarios = new ArrayList<>();
        
        
        try {
            
            statement = conexao.prepareStatement("SELECT * FROM login");           
            result = statement.executeQuery();
            
            while (result.next()){
                Usuario user = new Usuario();
                
                user.setId_user(result.getInt("id"));
                user.setLogin(result.getString("user_login"));
                user.setSenha(result.getString("senha"));
                usuarios.add(user);
                
            }
            
           // JOptionPane.showMessageDialog(null, "Programa carregado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao carregar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuarios;
       
    }
    public void createUser(Usuario user){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("INSERT INTO login (user_login, senha) VALUES (?,?)");            
            statement.setString(1,user.getLogin());
            statement.setString(2,user.getSenha());
            
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "salvo com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao salvar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
    public void updateUser(Usuario user){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        
        try {
            
            statement = conexao.prepareStatement("UPDATE login SET user_login=?,senha=? where id=?");
            
            statement.setString(1,user.getLogin());           
            statement.setString(2,user.getSenha());
            statement.setInt(3,user.getId_user());
                        
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao Atualiazar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
    public void deleteUser (Usuario user){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("Delete from login where id=?");
            statement.setInt(1,user.getId_user());
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "Excluido com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao Excluir");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao(conexao, statement);
        }
    }
}
