/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;


import cadastros.Fornecedor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author dabvi
 */
public class DaoFornecedor {
    
    public static void createFornecedor(Fornecedor fornecedor){
        Connection conexao = ConexaoBD.criarConexao();        
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("INSERT INTO fornecedor (nome_fornecedor,razao_social_fornecedor,telefone_forn,email_forn)VALUES(?,?,?,?)");
            statement.setString(1, fornecedor.getNome_fantasia());
            statement.setString(2, fornecedor.getRazao_social());
            statement.setString(3, fornecedor.getTelefone());
            statement.setString(4, fornecedor.getEmail());
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "salvo com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao salvar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao((Connection) conexao, statement);
        }
    }
    
    public List<Fornecedor> showGradeFornecedor(){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Fornecedor> forn = new ArrayList<>();
        
        
        try {
            
            statement = conexao.prepareStatement("SELECT * FROM fornecedor");           
            result = statement.executeQuery();
            
            while (result.next()){
                Fornecedor fornecedor = new Fornecedor();                
                fornecedor.setCodigo_fornecedor(result.getInt("codigo_fornecedor"));
                fornecedor.setNome_fantasia(result.getString("nome_fornecedor"));
                fornecedor.setRazao_social(result.getString("razao_social_fornecedor"));
                fornecedor.setTelefone(result.getString("telefone_forn"));
                fornecedor.setEmail(result.getString("email_forn"));                
                forn.add(fornecedor);
                
            }
            
           // JOptionPane.showMessageDialog(null, "Programa carregado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao carregar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return forn;
               
    }
    
    public List<Fornecedor> showGradeFornecedorbusca(String nome){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        ResultSet result = null;
        List<Fornecedor> forn = new ArrayList<>();
        
        
        try {
            
            statement = conexao.prepareStatement("SELECT * FROM fornecedor where nome_fornecedor LIKE ?");
            statement.setString(1,"%"+nome+"%");
            result = statement.executeQuery();
            
            while (result.next()){
                Fornecedor fornecedor = new Fornecedor();                
                fornecedor.setCodigo_fornecedor(result.getInt("codigo_fornecedor"));
                fornecedor.setNome_fantasia(result.getString("nome_fornecedor"));
                fornecedor.setRazao_social(result.getString("razao_social_fornecedor"));
                fornecedor.setTelefone(result.getString("telefone_forn"));
                fornecedor.setEmail(result.getString("email_forn"));                
                forn.add(fornecedor);
                
            }
            
           // JOptionPane.showMessageDialog(null, "Programa carregado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao carregar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return forn;
               
    }
            
    public static void deleteFornecedor(Fornecedor fornecedor){
        
        Connection conexao = ConexaoBD.criarConexao();
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("delete from fornecedor where codigo_fornecedor = ?");
            statement.setInt(1, fornecedor.getCodigo_fornecedor());
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "Excluido com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao excluir");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao((Connection) conexao, statement);
        }
        
        
    }    
    
   public static void updateFornecedor(Fornecedor fornecedor){
        Connection conexao = ConexaoBD.criarConexao();        
        PreparedStatement statement = null;
        
        try {
            
            statement = conexao.prepareStatement("update fornecedor set nome_fornecedor=?,razao_social_fornecedor=? ,telefone_forn=? ,email_forn=? where codigo_fornecedor = ? ");
            statement.setString(1, fornecedor.getNome_fantasia());
            statement.setString(2, fornecedor.getRazao_social());
            statement.setString(3, fornecedor.getTelefone());
            statement.setString(4, fornecedor.getEmail());
            statement.setInt(5,fornecedor.getCodigo_fornecedor());
            
            statement.executeUpdate();
            
            
            
            JOptionPane.showMessageDialog(null, "salvo com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao salvar");
            Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            ConexaoBD.fechaConexao((Connection) conexao, statement);
        }
    }
    
}
