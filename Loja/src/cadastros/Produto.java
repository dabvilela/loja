/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastros;

import java.util.Date;

/**
 *
 * @author dabvi
 */
public class Produto {

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    private String descricao;     
    private int codigo_produto;
    private double preco_venda;
    private double preco_custo;
    private Date data_validade;
    private double estoque;
    private int codigo_fornecedor_prod;

    
    public int getCodigo_produto() {
        return codigo_produto;
    }

   
    public void setCodigo_produto(int codigo_produto) {
        this.codigo_produto = codigo_produto;
    }

   
    public double getPreco_venda() {
        return preco_venda;
    }

   
    public void setPreco_venda(double preco_venda) {
        this.preco_venda = preco_venda;
    }

   
    public double getPreco_custo() {
        return preco_custo;
    }

    
    public void setPreco_custo(double preco_custo) {
        this.preco_custo = preco_custo;
    }

    
    public Date getData_validade() {
        return data_validade;
    }

   
    public void setData_validade(Date data_validade) {
        this.data_validade = data_validade;
    }

    
    public double getEstoque() {
        return estoque;
    }

    
    public void setEstoque(double estoque) {
        this.estoque = estoque;
    }

    
    public int getCodigo_fornecedor_prod() {
        return codigo_fornecedor_prod;
    }

    
    public void setCodigo_fornecedor_prod(int codigo_fornecedor_prod) {
        this.codigo_fornecedor_prod = codigo_fornecedor_prod;
    }

   
   
    
}
