/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastros;

/**
 *
 * @author dabvi
 */
public class Usuario {
    
    private int id_user;
    private String login;
    private String senha;
    
   
    public int getId_user() {
        return id_user;
    }

    
    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

   
    public String getLogin() {
        return login;
    }

   
    public void setLogin(String login) {
        this.login = login;
    }

    
    public String getSenha() {
        return senha;
    }

    
    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
}
