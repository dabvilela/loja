/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastros;

/**
 *
 * @author dabvi
 */
public class Fornecedor {
    
    private int codigo_fornecedor;
    private String nome_fantasia;
    private String razao_social;
    private String telefone;
    private String email;

    
    public int getCodigo_fornecedor() {
        return codigo_fornecedor;
    }

    
    public void setCodigo_fornecedor(int codigo_fornecedor) {
        this.codigo_fornecedor = codigo_fornecedor;
    }

   
    public String getNome_fantasia() {
        return nome_fantasia;
    }

   
    public void setNome_fantasia(String nome_fantasia) {
        this.nome_fantasia = nome_fantasia;
    }

    
    public String getRazao_social() {
        return razao_social;
    }

    
    public void setRazao_social(String razao_social) {
        this.razao_social = razao_social;
    }

   
    public String getTelefone() {
        return telefone;
    }

    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    
    public String getEmail() {
        return email;
    }

    
    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}
