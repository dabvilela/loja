/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastros;

import java.util.Date;
import java.util.List;

/**
 *
 * @author dabvi
 */
public class Cliente {
    
    
    private int codigo_cliente;
    private String nome ;
    private Date data_nascimento;
    private String endereco;
    private String telefone;
    private String email;
    

   
    public int getCodigo_cliente() {
        return codigo_cliente;
    }

   
    public void setCodigo_cliente(int codigo_cliente) {
        this.codigo_cliente = codigo_cliente;
    }

    
    public String getNome() {
        return nome;
    }

  
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    public Date getData_nascimento() {
        return data_nascimento;
    }

    
    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    
    public String getEndereco() {
        return endereco;
    }

  
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

   
    public String getTelefone() {
        return telefone;
    }

   
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

   
    public String getEmail() {
        return email;
    }

   
    public void setEmail(String email) {
        this.email = email;
    }
    
    public void add(Cliente cliente) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void add(List<Cliente> clientes) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }        
}
